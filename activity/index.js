let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}


///////////////////////////////////////////////
introduce = (student) => {
	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old.`)
	console.log(`I study the following courses: ${student.classes}.`)
}
introduce(student1);
introduce(student2);


getCube = (num) => console.log(num ** 3);
getCube(10);


///////////////////////////////////////////////
let numArr = [15,16,32,21,21,2];
let [num1,num2,num3] = numArr;
console.log(num1);
console.log(num2);
console.log(num3);

numArr.forEach(num=>console.log(num))

let numsSquared = numArr.map(num => num ** 2)
console.log(numsSquared);


///////////////////////////////////////////////
class Movie{
	constructor(title,director,producer,year,cast){
		this.title = title
		this.director = director
		this.producer = producer
		this.year = year
		this.cast = cast
	}
}

let movieSample1 = new Movie('Movie1', 'Director1', 'Producer1', 2022, ['Actor1', 'Actor2']);
let cityOfGod = new Movie('City of God', 'Fernando Meirelles', 'Andrea Barata Ribeiro', 2002, ['Alexandre Rodrigues', 'Leandro Firmino']);

console.log(movieSample1);
console.log(cityOfGod);



