//ES6 Updates

//Exponents
/*
	Math.pow() to get the result of a number raised to a given exponent.

	syntax:
	Math.pow(base,exponent)
*/
	/*let fivePowerOf3 = Math.pow(5,3);
	console.log(fivePowerOf3);//125*/

	
//Exponent operaters (**) = allow us to get the result of a number raised to a given exponent

	let fivePowerOf3 = 5**3;
	console.log(fivePowerOf3);//125

//MiniAct
/*
	let fivePowerOf2 = Mathpow(5,2);
	let fivePowerOf4 = Math.pow(4,5);
	let lettwoPowerOf2 = Math.pow(2,2);
*/


let fivePowerOf2 = 5 ** 2;
let fivePowerOf4 = 5 ** 4;
let twoPowerOf2 = 2 ** 2;

console.log(fivePowerOf2);
console.log(fivePowerOf4);
console.log(twoPowerOf2);


function squareRootCheck(num){
	return num**.5
}

let squareRootOf4 = squareRootCheck(4);
console.log(squareRootOf4);


//Template Literals
/*
	An ES6 Update to creating strings. Represented by backticks (``)

	(''), ("") - string literals
	`` - backticks / template literals
*/

let sampleString1 = `Charlie`;
let sampleString2 = `Joy`;
console.log(sampleString1);
console.log(sampleString2);

let combinedString = `${sampleString1} ${sampleString2}`
console.log(combinedString);
//We can create a single string to combine our variables. This is with the use of template literals('') and by embedding JS expressions and variables in the string ${} or placeholders.


//Template Literals with JS Expressions

let num1 = 15;
let num2 = 3;


//What if we want to say a sentence in the console like this
	//"The result of 15 plus 3 is 18"

console.log(`The result of ${num1} plus ${num2} is ${num1+num2}`)

let resultOf15ToPowerOf2 = 15**2
let sentence2 = `The result of 15 to the power of 2 is ${resultOf15ToPowerOf2}`
console.log(sentence2);

let resultOf3Times6 = 3 * 6;
let sentence3 = `The result of 3 times 6 is ${resultOf3Times6}`
console.log(sentence3);

/*
function product(num1, num2){
	return console.log(`${num1} x ${num2} = ${num1 * num2}`)
}
product(1,2);
*/

/*
function power(num1,num2){
	return console.log(`${num1} to the power of ${num2} is ${num1**num2}.`)
}
power(2,2)
*/

//Array Destructing
	//When we destructure an array, what we essentially do is to save array items in avariables

let array = ['Kobe', 'Lebron', 'Jordan'];

/*let goat1 = array[0];
console.log(goat1);

let goat2 = array[1];
let goat3 = array[2];*/

let [goat1,goat2,goat3] = array;
console.log(goat1)
console.log(goat2)
console.log(goat3)

let array2 = ['Curry', 'Lillard', 'Paul', 'Irving'];

let [pg1,pg2,pg3,pg4] = array2;

console.log(pg1);
console.log(pg2);
console.log(pg3);
console.log(pg4);

/*
	In arrays, the order of items/elements is important and that goes the same to destructuring.

	The order of variables where we want to save our array items is also important.

	(,,) You can skip an item by adding another separator (,) and by not providing a variable to save the item we want to skip
*/

let array3 = ['Jokic', 'Embiid', 'Anthony-Towns', 'Gobert']
let [center1,,center3] = array3;
console.log(center1);//Jokic
console.log(center3);//Anthony-Towns = skipped Embiid (,,)

let array4 = ['Draco Malfoy', 'Hermione Granger', 'Harry Potter', 'Ron Weasley', 'Professor Snape'];
let [,gryffindor1,gryffindor2,gryffindor3,] = array4;

console.log(gryffindor1);//hermione
console.log(gryffindor2);//harry
console.log(gryffindor3);//ron

/*
	Destructuring can allow us to destructure an array and save items in variables and also into constants.
	 
	let/const [variable1,variable2] = arrayName;
*/

const [slytherin1,,,,slytherin2] = array4

console.log(slytherin1);//draco
console.log(slytherin2);//snape

gryffindor1 = "Emma Watson";
console.log(gryffindor1)
/*
slytherin1 = "Tom Felton"
console.log(slytherin1)
*/

//Object Destructuring
	//will allow us to destructure an object by saving/adding the values of an object's property into respective variables


let pokemon = {
	name: 'Blastoise',
	level: 40,
	health: 80,
}

let sentence4 = `The pokemon's name is ${pokemon.name}.`
let sentence5 = `It is a level ${pokemon.level} pokemon.`
let sentence6 = `It has at least ${pokemon.health}.`

console.log(sentence4);
console.log(sentence5);
console.log(sentence6);


//We can save the values of an object's properties into variables.
//by Object Destructuring

let {health,name,level,attack} = pokemon;
console.log(health);
console.log(name);
console.log(level);
console.log(attack);//undefined - because there is no attack property in our pokemon

/*
	in Array Destructuring, order was important andthe name of the variable we save our items in is not important

	in Object Destructuring, order is not important however the name of the variables should match the properties of the object. Else it may result to undefined

*/


let person = {
	name: 'Paul Phoenix',
	age: 31,
	birthday: "January 12, 1991"
}

//you can destructure in the parameter
function greet({name,age,birthday}){

	//let {name,age,birthday} = p

	console.log(`Hi! My name is ${name}.`)
	console.log(`I am ${age} years old`)
	console.log(`My birthday is on ${birthday}.`)
}
greet(person);

/*

let actors = ['Tom Hanks', 'Leonardo Dicarpio', 'Anthony Hopkins', 'Ryan Reynolds'];
let director = {
	name: 'Alfred Hitchcock',
	birthday: 'August 13, 1889',
	isActive: false,
	movies: ['The Birds', 'Psycho', 'North by Northwest']
};

let {actor1,actor2,actor3} = actor;
function displayDirector(person){

	let {name,birthday,movie,nationality} = person;

	console.log("The Director's name is $name");
	console.log(`He was born on ${person.birthday}`);
	console.log(`His movies include`);
	console.log(movies);
}

console.log(actor1)
console.log(actor2)
console.log(actor4)

displayDirector(director)

*/


let actors = ['Tom Hanks', 'Leonardo Dicarpio', 'Anthony Hopkins', 'Ryan Reynolds'];
let director = {
	name: 'Alfred Hitchcock',
	birthday: 'August 13, 1889',
	isActive: false,
	movies: ['The Birds', 'Psycho', 'North by Northwest']
};

let [actor1,actor2,,actor4] = actors;

function displayDirector({name,birthday,movies}){
	console.log(`The Director's name is ${name}`);
	console.log(`He was born on ${birthday}`);
	console.log(`His movies include: ${movies}`);
}

console.log(actor1);
console.log(actor2);
console.log(actor4);

displayDirector(director);


//Arrow functions
/*
	are an alternative of writing/declaring functions. However, there are significant difference between our regular/traditional function and arrow function
*/

	//regular/traditional functon
	function displayMsg(){
		console.log('Hello, World!')
	}
	displayMsg();

	//arrow functions
	const hello = () => {
		console.log('Hello from Arrow!')
	}
	hello();

	/*
	function greet(personParams){
		console.log(`Hi, ${personParams.name}!`)
	}
	greet(person);
	*/

	const greet2 = (personParams) => {
		console.log(`Hi, ${personParams.name}!`)
	}
	greet2(person);

	//Anonymous functions in array methods
	/*
		It allows us to iterate/loop over all items in an array.

		An anonymous function is added so we can perform tasks per item in the array

		This anonymous function receives the current item being iterated/looped
	*/

		/*
		actors.forEach(function(actor){
			console.log(actor)
		})
		*/

		//actors.forEach(function(actor)console.log(actor))

		//actors.forEach(actor=>console.log(actor))

		//Note: traditional functions cannot work without the curly braces.
		
		//Note: However, arrow function can BUT it has to be a one-liner
		actors.forEach(actor=>console.log(actor))

		//.map() is similar to forEach wherein we can iterte over all items in our array.
		//The difference is we can return items from a map and create a new array out of it.

		let numberArr = [1,2,3,4,5]

		/*
		let multipliedBy5 = numberArr.map(function(number){
			return number*5
		})
		*/

		/*
		let multipliedBy5 = number.Arr.map(number=>{return number*5})
		*/

		let multipliedBy5 = numberArr.map(number=>number*5)
		console.log(multipliedBy5);//5,10,15,20,25
		//arrow functions do not need the return keyword to return a value. This is called implicit return
		//when arrow functions have a curly brace, it will need to have a return keyword to return a value.



		//Implicit Return for Arrow functions

	/*
		function addNum(num1,num2){
			return num1+num2
		}

		let sum1 = addNum(5,10)
		console.log(sum1)//15
	*/

		const subtractNum = (num1,num2) => num1 - num2;
		let difference1 = subtractNum(45,15);
		console.log(difference1)//30
		//Even without a return keyword, arrow functions can return a value. So long as its code block is not wrapped with{}

		addNum = (num1,num2) => num1 + num2;

	/*
		let sumExample = addNum(50,10)
		console.log(sumExample)
	*/
		console.log(addNum(50,10))

		
		//(this) keyword in a method

	
		let protagonist = {
			name: 'Cloud Strife',
			occupation: 'SOLDIER',
			greet: function(){
				console.log(this);//this refers to the object where the method is in or methods created as traditional functions.
				console.log(`Hi! I am ${this.name}`)
			},
			introduceJob: () => {
				console.log(this.occupation)
				console.log(this)
			}
		}

		protagonist.greet();
		protagonist.introduceJob();//undefined - because this in a arrow function refers to the global window object
		//you cannot use this in a arrow method (method using arrow function)

	//Class-Based Objects Blueprints
		//In JS, Classes are templates/blueprints to creating objects
		//We can create objects out of the use of Classes.
		//Before the introduction of Classes in JS, we mimic this behavior to create objects out of templates with the use of constructor functions.

	//Constructor function
		function Pokemon(name,type,level){
			this.name =  name;
			this.type = type;
			this.level = level;
		}

		let pokemon1 = new Pokemon('Sandshrew', 'Ground', 15);
		console.log(pokemon1)

	//Es6 Class Creation
		//we have a more distinctive way of creating classes instead of just our functions.

		class Car {
			constructor(brand,name,year){
				this.brand = brand
				this.name = name
				this.year = year
			}
		}

		let car1 = new Car('Toyota', 'Vios', '2002');
		console.log(car1);

		





		








